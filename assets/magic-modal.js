(function(global, $) {
  "use strict";

  global.magicModal = {
    open,
    close,
    goToSlide
  };

  function open({ width = "100vw", height = "100vh", data = {} } = {}) {
    if (!data.tagblog) {
      throw "Bad data.";
      return;
    }

    const modalTemplate = `<div class="si-modal-overlay"></div>
    <div class="si-modal-wrap">
        <a class="si-modal__closeButton">
            <i class="fa fa-close"></i>
        </a>
        <div class="si-modal" id="modalTarget1">
            <div class="si-slider"><div class="si-slider__posts"></div></div>
            <nav class="si-slider__nav">
              <ul></ul>
            </nav>
        </div>
    </div>`;
    let modalElement = $(modalTemplate);

    // $("body").append(modalTemplate);

    const tagblog = data.tagblog;
    const imagePosts = tagblog.filter(item => item.item_type == "lenzor");
    const textPosts = tagblog.filter(item => item.item_type == "mihanblog");
    injectTextPosts(modalElement, textPosts);
    injectImagePosts(modalElement, imagePosts);

    $("html").addClass("si-modal-opened");
    modalElement.on("click", handleWrapClick);
    modalElement.find(".si-modal__closeButton").on("click", () => {
      close({ target: modalElement });
    });
    modalElement.find(".si-modal").css({ width, height });

    for (let i = 0; i < tagblog.length; i++) {
      modalElement.find(".si-slider__nav ul").append(`<li><a value="${i}">${i+1}</a></li>`);
    }
    modalElement.find(".si-slider__nav li a").on("click", handleNavClick);

    $("body").append(modalElement);
    const sliderWidth = modalElement.find(".si-modal").innerWidth();
    const postsElement = modalElement.find(".si-slider__posts");
    postsElement.css({ width: sliderWidth * tagblog.length });
    modalElement.find(".si-slider__post").css({ width: sliderWidth });
    
    function handleWrapClick(event) {
      if ($(event.target).parents(".si-modal").length) {
        return;
      }
      close();
    }
    
    function handleNavClick(event) {
      let slideNumber = $(event.target).attr("value") || 0;
      goToSlide({postsElement, slideNumber, unitWidth: sliderWidth});
      
    }

    function injectImagePosts(parent, posts) {
      const pElem = parent.find(".si-slider__posts");
      posts.forEach(post => {
        pElem.append(`
          <div class="si-slider__post" id="imagePost-${post.id}">
            <div class="si-slider__postContent">
              <h2>${post.title}</h2>
              <img src="${post.descr}" />
            </div>
          </div>
        `);
      });
    }
    function injectTextPosts(parent, posts) {
      const pElem = parent.find(".si-slider__posts");
      posts.forEach(post => {
        pElem.append(`
          <div class="si-slider__post" id="imagePost-${post.id}">
            <div class="si-slider__postContent">
              <h2>${post.title}</h2>
              <p>${post.descr}</p>
            </div>
          </div>
        `);
      });
    }
  }

  function close() {
    $(".si-modal-wrap, .si-modal-overlay").remove();
    $("html").removeClass("si-modal-opened");
  }

  function goToSlide({ slideNumber = 0, postsElement, unitWidth }) {
    console.log("postsElement", postsElement);
    // TODO: USE TRANSFORM
    postsElement.css({ "margin-right": -slideNumber * unitWidth });
  }
})(window, jQuery);
